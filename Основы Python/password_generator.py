import string
import random


def generator(length_pass):
    eng_str = string.ascii_letters  # 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    num_str = '0123456789'

    # список с русскими символами
    a, A = ord('а'), ord('А')
    rus_str = ''.join([chr(i) for i in range(a, a + 32)])
    rus_str += ''.join([chr(i) for i in range(A, A + 32)])
    special_symb = r'+-/*!&$#?=@<>'
    full_str = eng_str + num_str
    password = ''
    if not length_pass:
        full_str += rus_str + special_symb
        length_pass = random.randint(6, 20)

    for i in range(length_pass):
        password += full_str[random.randint(0, len(full_str)-1)]
    return password


if __name__ == "__main__":
    while True:
        count_pas = input("Введите необходимое число паролей: ")
        if count_pas.isdigit():
            break
        else:
            print("Неправильный ввод! Введите число.")
    while True:
        len_pas = input("Введите длину пароля (0 - автоматическая длинна. Используется больший диапозон символов): ")
        if len_pas.isdigit():
            if int(len_pas) and (int(len_pas) < 6 or int(len_pas) > 20):
                print("Длина пароля должна быть больше 6 и меньше 20 символов.")
            else:
                break
        else:
            print("Неверный ввод, попробуйте ещё раз!")

    print("Количество паролей -", count_pas,
          "\nДлина пароля -", len_pas if int(len_pas) else "определится случайно")
    for pas in range(int(count_pas)):
        print(generator(int(len_pas)))
