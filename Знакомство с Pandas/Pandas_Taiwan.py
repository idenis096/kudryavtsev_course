import pandas as pd


def new_col(limit_bal):
    if limit_bal < 10001: return "A"
    if limit_bal < 100001: return "B"
    if limit_bal < 200001: return "C"
    if limit_bal < 400001: return "D"
    if limit_bal < 700001: return "E"
    if limit_bal > 700001: return "F"


if __name__ == "__main__":
    taiwan_data = pd.read_csv("UCI_Credit_Card.csv")
    # В переменную dtype_set помещаем множество из типов данных
    print(taiwan_data)
    dtype_set = set(taiwan_data.dtypes)
    print("Файл UCI_Credit_Card.csv содержит следующие типы переменных: {}, {}".format(*dtype_set))
    print("\nКоличество пропусков в данных =", sum(list(taiwan_data.isnull().sum())))
    print("\nБазовые статистики:")
    print(taiwan_data.describe())

    # Фильтруем таблицу по половому признаку, выделяем мужчин
    all_male = taiwan_data[taiwan_data['SEX'] == 1]
    # Полученную таблицу фильтруем по параметру EDUCATION и с помощью метода shape
    # узнаём количество мужчин с университетским образованием (за счёт подчета количества строк)
    print("Число мужчин с университетским образованием = ", all_male[
        all_male["EDUCATION"] == 2].shape[0])

    # Фильтруем таблицу по половому признаку, выделяем женщин
    all_female = taiwan_data[taiwan_data['SEX'] == 2]
    # Полученную таблицу фильтруем по параметру EDUCATION и с помощью метода shape
    # узнаём количество женщин с университетским образованием (за счёт подчета количества строк)
    print("Число женщин с университетским образованием = ", all_female[
        all_female["EDUCATION"] == 2].shape[0])

    '''Сгрупировать по столбцу "default.payment.next.month" 
    и посчитать медиану для всех показателей начинающихся на BILL_ и PAY_'''

    # Занесем в список все поля начинающиеся на BILL_ и PAY_.
    # Для этого воспользуемся генератором и методом startswith
    filter_BILL_PAY = [BILL_PAY for BILL_PAY in taiwan_data if
                       (BILL_PAY.startswith('BILL_') or BILL_PAY.startswith('PAY_'))]

    # Сгруппируем получившийся датасет по столбцу "default.payment.next.month" и посчитаем медиану
    print(taiwan_data.groupby("default.payment.next.month")[filter_BILL_PAY].median())

    '''Построить сводную таблицу (pivot table) по столбцам SEX, EDUCATION, MARRIAGE'''
    print(taiwan_data.pivot_table('SEX', ['EDUCATION', 'MARRIAGE']))

    '''Создать новый строковый столбец в DataFrame, который:
        *   принимает значение A, если значение LIMIT_BAL <=10000,
        *   принимает значение B, если значение LIMIT_BAL <=100000 и >10000,
        *   принимает значение C, если значение LIMIT_BAL <=200000 и >100000,
        *   принимает значение D, если значение LIMIT_BAL <=400000 и >200000,
        *   принимает значение E, если значение LIMIT_BAL <=700000 и >400000,
        *   принимает значение F, если значение LIMIT_BAL >700000
    '''
    # Создаём список с помощью генератора. Вызывается функция, в которую передаётся значение ячейки
    # в зависимости от значения ячейки возвращается один из символов: ABCDEF
    new = [new_col(x) for x in list(taiwan_data.loc[:, 'LIMIT_BAL'])]
    taiwan_data['NEW'] = new
    print("Обновленный список с добавленной колонкой")
    print(taiwan_data)
