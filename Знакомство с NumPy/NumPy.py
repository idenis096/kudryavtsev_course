import numpy as np


if __name__ == "__main__":
    fname = "boston_houses.csv"
    boston_houses = np.loadtxt(fname, dtype='float', delimiter=',', skiprows=1)

    # axis=0 - указывает на то, что необходимо подсчитать значение в столбцах. axis=1 - для строк
    print("Средние значения столбцов =", boston_houses.mean(axis=0))
    print("Максимальное значения столбцов =", boston_houses.max(axis=0))
    print("Минимальное значения столбцов =", boston_houses.min(axis=0))
    print("Минимальное значения столбцов =", boston_houses.sum(axis=0))

